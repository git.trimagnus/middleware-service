<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();  
});

$router->get('test', 'ExampleController@test');

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    // $router->post('/login', 'V1\AuthController@login');

    // $router->group(['middleware' => 'auth'], function () use ($router) {
    // $router->get('me', 'V1\AuthController@me');

    $router->get('nodes/{deviceEui}', 'V1\NodeController@show');
    $router->get('gateways/{mac}', 'V1\GatewayController@show');

    // $router->get('users', 'V1\UserController@index');
    // $router->post('users', 'V1\UserController@store');
    // $router->get('users/{id}', 'V1\UserController@show');
    // $router->put('users/{id}', 'V1\UserController@update');
    // $router->delete('users/{id}', 'V1\UserController@destroy');

    // });
});





