<?php

namespace App\Repositories\Api;

class NodeRepository {

    public function findByDeviceEUI($deviceEUI)
    {
        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => AuthRepository::getToken(),
                'Content-Type' => 'application/json'
            ];
            $res = $client->get(env('NS_API_HOST').'/api/nodes/'.$deviceEUI,[
                'headers' => $headers,
            ]);
            $statusCode = $res->getStatusCode();
            $response = json_decode($res->getBody());

            $result = [
                'status' => $statusCode,
                'data' => $response
            ];
            return $result;

        } catch (\Exception $e) {
            $res = $e->getResponse();
            $statusCode = $res->getStatusCode();
            $response = json_decode($res->getBody());

            $result = [
                'status' => $statusCode,
                'data' => $response
            ];
            return $result;

        }
    }
}