<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Api\NodeRepository;

class NodeController extends Controller
{
    protected $node;

    public function __construct(NodeRepository $node)
    {
        $this->node = $node;
    }

    public function show($deviceEui)
    {
        $result = $this->node->findByDeviceEUI($deviceEui);

        return response()->json($result, $result['status']);
    }
}